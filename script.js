document.addEventListener("DOMContentLoaded", function() {
    const chatbox = document.getElementById("chatbox");
    const messageInput = document.getElementById("message");
    const sendButton = document.getElementById("sendBtn");
    let editMode = false;
    let editMessageId = null;
    const messages = {};

    function addMessageToChatbox(id, text) {
        const messageElement = document.createElement("div");
        messageElement.setAttribute("id", id);
        messageElement.classList.add("message");
        messageElement.textContent = "Você: " + text;

        const editButton = document.createElement("span");
        editButton.classList.add("edit-btn");
        editButton.textContent = "Editar";
        editButton.addEventListener("click", function() {
            editMode = true;
            editMessageId = id;
            messageInput.value = messages[id].text;
        });

        const deleteButton = document.createElement("span");
        deleteButton.classList.add("delete-btn");
        deleteButton.textContent = "Excluir";
        deleteButton.addEventListener("click", function() {
            delete messages[id];
            chatbox.removeChild(messageElement);
        });

        messageElement.appendChild(editButton);
        messageElement.appendChild(deleteButton);
        chatbox.appendChild(messageElement);
        chatbox.scrollTop = chatbox.scrollHeight;
    }

    sendButton.addEventListener("click", function() {
        const messageText = messageInput.value.trim();

        if (editMode && editMessageId) {
            // Edit existing message
            messages[editMessageId].text = messageText;
            const messageElement = document.getElementById(editMessageId);
            messageElement.textContent = "Você: " + messageText;
            editMode = false;
            editMessageId = null;
        } else {
            // Send new message
            if (messageText !== "") {
                const messageId = "msg-" + Date.now();
                messages[messageId] = {
                    text: messageText,
                    timestamp: Date.now()
                };
                addMessageToChatbox(messageId, messageText);
                messageInput.value = "";
            }
        }
    });

    messageInput.addEventListener("keyup", function(event) {
        if (event.key === "Enter") {
            sendButton.click();
        }
    });
});
